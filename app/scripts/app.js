'use strict';

/**
 * @ngdoc overview
 * @name bfTechTestNgApp
 * @description
 * # bfTechTestNgApp
 *
 * Main module of the application.
 */
angular
  .module('bfTechTestNgApp', [
    'ui.router'
  ])
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('main', {
          url: '/',
          templateUrl: 'views/main.view.html',
      })
      .state('read-me', {
          url: '/read-me',
          templateUrl: 'views/read-me.view.html',
      });
}]);
