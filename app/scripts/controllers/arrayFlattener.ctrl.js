'use strict';

/**
 * @ngdoc function
 * @name bfTechTestNgApp.controller:arrayFlattener
 * @description
 * # arrayFlattener
 * Controller of the bfTechTestNgApp
 */
angular.module('bfTechTestNgApp')
  .controller('arrayFlattener', function ($scope, $http) {

  	$scope.arrayToFlatten = [ 1, [ 2, [ 3 ] ], 4 ];
  	console.log('Array to flatten:');
  	console.log($scope.arrayToFlatten);

  	var flattenedArray = [];

  	// Function to flatten an array
  	$scope.flattenArray = function(array) {  
  		// Check it is indeed an array that's input
  		if(Array.isArray(array)) {
	  		// For each item in array
	  		angular.forEach(array, function(value) {
	  			// Is the item is an array? 
		  		if(Array.isArray(value)) {
		  			// Clone function and invoke
  					var flattenedArrayCopy = angular.copy($scope.flattenArray);
		  			flattenedArrayCopy(value);
		  		} else {
		  			// Otherwise, add item to flattened array
		  			flattenedArray.push(value);
		  		}
		  	});
		  	return flattenedArray;
		  } else {
		  	return 'Error: please input an array';
		  }
  	}

  	$scope.resetAndFlattenArray = function(array) {
  		flattenedArray = [];
  		return $scope.flattenArray(array);
  	}

  	// Invoke function to flatten array
  	flattenedArray = $scope.resetAndFlattenArray($scope.arrayToFlatten);
  	console.log('->');
  	console.log('Flattened array:');
  	console.log(flattenedArray);
  	console.log('----------------');

  });
