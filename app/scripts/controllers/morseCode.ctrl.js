'use strict';

/**
 * @ngdoc function
 * @name bfTechTestNgApp.controller:morseCode
 * @description
 * # morseCode
 * Controller of the bfTechTestNgApp
 */
angular.module('bfTechTestNgApp')
  .controller('morseCode', function ($scope, $http) {

  	$scope.sentence = 'I AM IN TROUBLE';
    $scope.filePath = 'text.txt';
    $scope.type = 'txt';

  	var morseCodeRef = {
			'A':  '.-',
			'B':  '-...',
			'C':  '-.-.',
			'D':  '-..',
			'E':  '.',
			'F':  '..-.',
			'G':  '--.',
			'H':  '....',
			'I':  '..',
			'J':  '.---',
			'K':  '-.-',
			'L':  '.-..',
			'M':  '--',
			'N':  '-.',
			'O':  '---',
			'P':  '.--.',
			'Q':  '--.-',
			'R':  '.-.',
			'S':  '...',
			'T':  '-',
			'U':  '..-',
			'V':  '...-',
			'W':  '.--',
			'X':  '-..-',
			'Y':  '-.--',
			'Z':  '--..',
			'0':  '-----',
			'1':  '.----',
			'2':  '..---',
			'3':  '...--',
			'4':  '....-',
			'5':  '.....',
			'6':  '-....',
			'7':  '--...',
			'8':  '---..',
			'9':  '----.',
			'Fullstop': '.-.-.-',
			'Comma': '--..--',
  	},
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''),
    wordSep = '/',
    letterSep = '|',
    sentenceRegEx = /^[a-zA-Z\s]*$/,
    obfuscatedMorseCode = '',
    successiveCounter = 1,
    errorMessage = 'Error: please only use letters and spaces';

  	// Function to turn a sentence into morse code
  	$scope.convertToMorseCode = function(sentence) {
  		var morseCodeEquivalent = '';
      if(sentenceRegEx.test(sentence)) {
    		// Ensure sentence is uppercase
    		sentence = sentence.toUpperCase();
    		// Convert each character
    		angular.forEach(sentence, function(value, index){
    			if(value === ' ') {
    				// Add word seperator
    				morseCodeEquivalent = morseCodeEquivalent + wordSep;
    			} else {
  	  			morseCodeEquivalent = morseCodeEquivalent + morseCodeRef[value];
  	  			// Add a letter seperator - but void at the end of a word and at end of whole sentence
  	  			if(sentence.charAt(index + 1) !== ' ' && index !== sentence.length - 1) {
  	  				morseCodeEquivalent = morseCodeEquivalent + letterSep;
  	  			}
  	  		}
    		});
  		  $scope.morseCode = morseCodeEquivalent;
        return morseCodeEquivalent;
      } else {
        return errorMessage;
      }
  	};

  	// Function to obfuscate morse code
  	$scope.obfuscateMorseCode = function(morseCodeExpression) {
      // Check we're not being passed an error
      if(morseCodeExpression !== errorMessage) {
    		obfuscatedMorseCode = '';
    		angular.forEach(morseCodeExpression, function(value, index){
    			// If it's a word or letter seperator, pass the value on
    			if(value === wordSep || value === letterSep) {
    				obfuscatedMorseCode = obfuscatedMorseCode + value;
    				// Reset successive character count
    				successiveCounter = 1;
    			} else {
    				checkForRepeatAndObfuscateChar(morseCodeExpression, value, index);
    			}
    		});
    		return obfuscatedMorseCode;
      } else {
        return errorMessage;
      }
  	}

  	var checkForRepeatAndObfuscateChar = function(expression, character, index) {
  		var obfuscatedCharacter;
  		// If it's a repeat
  		if(expression.charAt(index - 1) === character) {
				successiveCounter ++;
				// Remove the previous character
				obfuscatedMorseCode = obfuscatedMorseCode.substring(0, obfuscatedMorseCode.length - 1);
			} else {
				// Reset counter if not a repeat
				successiveCounter = 1;
			}
  		// If it's a dash, use alphabet array
  		if(character === '-') {
  			obfuscatedCharacter = alphabet[successiveCounter - 1];
  		} else {
        obfuscatedCharacter = successiveCounter;
      }
  		obfuscatedMorseCode = obfuscatedMorseCode + obfuscatedCharacter;
  	}

  	$scope.convertAndObfuscate = function(dataToObfuscate, type) {
      $scope.reset();
      // If we're getting a text file, get it
      if(type === 'txt') {
        // Get text file
        $http.get(dataToObfuscate).then(function successCallback(data) {
          // Success
          dataToObfuscate = data.data;
          $scope.obfuscatedSentence = $scope.obfuscateMorseCode($scope.convertToMorseCode(dataToObfuscate));
        }, function errorCallback(data){
          // Error
          $scope.obfuscatedSentence = 'Error: file could not be found';
        });
      } else {
        // Or just run the functions with the sentence
  		  $scope.obfuscatedSentence = $scope.obfuscateMorseCode($scope.convertToMorseCode(dataToObfuscate));
      }
  	};

    // Reset function
    $scope.reset = function() {
      $scope.morseCode = '';
      $scope.obfuscatedSentence = '';
    }

  });
