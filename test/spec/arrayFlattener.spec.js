'use strict';

describe('In the arrayFlattener controller', function () {

  // load the controller's module
  beforeEach(module('bfTechTestNgApp'));

  var arrayFlattener,
      scope,
      arrayToFlatten = [ 1, [ 2, [ 3 ] ], 4, 5 ],
      error = 'Error: please input an array',
      testNonArray = function(type, value) {
        it('should return an error if a ' + type + ' is passed', function () {
          expect(scope.flattenArray(value)).toEqual(error);
        });
      },
      testNonArrayOnReset = function(type, value, scp, functionName) {
        it('should return an error if a ' + type + ' is passed', function () {
          expect(scope.resetAndFlattenArray(value)).toEqual(error);
        });
      };

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    arrayFlattener = $controller('arrayFlattener', {
      $scope: scope
    });
  }));

  it('should set the flattenArray variable to a nested array', function () {
    expect(scope.arrayToFlatten).toBeDefined();
  });

  describe('The $scope.flattenArray function', function () {
    it('should flatten the scope.arrayToFlatten nested array and push it to end of existing flattened array', function () {
      expect(scope.flattenArray(arrayToFlatten)).toEqual([1, 2, 3, 4, 1, 2, 3, 4, 5]);
    });

    testNonArray('string', 'foo');

    testNonArray('boolean', true);

    testNonArray('object', { 1: 1, 2:2 });
  });

  describe('The $scope.resetAndFlattenArray function', function () {
    it('should flatten the scope.arrayToFlatten nested array', function () {
      expect(scope.resetAndFlattenArray(arrayToFlatten)).toEqual([1, 2, 3, 4, 5]);
    });

    testNonArrayOnReset('string', 'foo');

    testNonArrayOnReset('boolean', true);

    testNonArrayOnReset('object', { 1: 1, 2:2 });
  });
});
