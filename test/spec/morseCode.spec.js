'use strict';

describe('In the morseCode controller:', function () {

  // load the controller's module
  beforeEach(module('bfTechTestNgApp'));

  var morseCode,
      scope,
      sentence = 'I AM IN TROUBLE',
      morseCodeExp = '../.-|--/..|-./-|.-.|---|..-|-...|.-..|.',
      obfuscation = '2/1A|B/2|A1/A|1A1|C|2A|A3|1A2|1',
      error = 'Error: please only use letters and spaces',
      testNonArrayOnConvert = function(type, value) {
        it('should return an error when converting a ' + type + ' to morse code', function () {
          expect(scope.convertToMorseCode(value)).toEqual(error);
        });
      };

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    morseCode = $controller('morseCode', {
      $scope: scope
    });
  }));

  it('should set the sentence scope variable', function () {
    expect(scope.sentence).toBeDefined();
  });

  describe('the $scope.convertToMorseCode function', function () {
    it('should convert the sentence to morse code', function () {
      expect(scope.convertToMorseCode(sentence)).toEqual(morseCodeExp);
    });

    it('should convert the sentence in lower case to morse code', function () {
      expect(scope.convertToMorseCode(sentence.toLowerCase())).toEqual(morseCodeExp);
    });

    testNonArrayOnConvert('number', '1');
    testNonArrayOnConvert('punctuation character', '.');
  });

  describe('the $scope.obfuscateMorseCode function', function () {
    it('should obfuscate the morse code', function () {
      expect(scope.obfuscateMorseCode(morseCodeExp)).toEqual(obfuscation);
    });

    it('should return the same error if the error is passed', function () {
      expect(scope.obfuscateMorseCode(error)).toEqual(error);
    });

  });

});
