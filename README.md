# Ben Fraser Tech test for Resource Guru | Nov 2017

http://megalodondesign.com/bf-tech-test/
boinfraser@gmail.com
07903664586
http://benfraserdesign.co.uk


Thanks for checking out my tech test for Resource Guru. 

The array flattening algorithm code is in app/scripts/controllers/arrayFlattener.ctrl.js. There is no input, but the code runs with the example provided and outputs the result in the console.

The morse code exercise is in app/scripts/controllers/morseCode.ctrl.js and runs in the browser. I have provided the option to provide an input via a .txt file or via a web form.

## About

 * http://megalodondesign.com/bf-tech-test/ to see final result

 * Or open the index.html file in the 'dist' directory in a web browser via a server

 * AngularJS application single page app

 * Styles are compiled in Less. The code is drawn from my own personal CSS framework I am developing - itself heavily inspired by Harry Robert's SASS architecture

 * Bower is used to manage dependencies

 * Grunt is used for task automation

 * Tests are run using Karma and Jasmine


## How to run this application (dev)

 * Install node based build dependencies

        npm install

 * Install bower components

        bower install

 * Run the development server with live watch

        grunt serve

 * Run unit tests

 				npm test

## How to build this application (dist)

 * Run a build (haven't had time to iron-out all issues here so the --force is necessary, sorry)

        grunt --force


## TODO
 
 * Get unit tests running using Grunt

 * Clean-up Grunt tasks 









 






